"""
  Pol Moreno

  10/11/2021

  ASIXc1A M03 UF1

    S'imprimeix una piràmide d'altura N de #
"""
from time import sleep

MAX_VOLTES = 5

for volta in range(MAX_VOLTES):
   print(volta + 1, "Mississipi")
   sleep(1)
