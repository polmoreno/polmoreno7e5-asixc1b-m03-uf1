"""
  Pol Moreno

  23/11/2021

  ASIXc1A M03 UF1

  Exercici 2

  Crea una aplicació que permeti endevinar un número. L'aplicació genera un nombre “aleatori” de l'1 al 100.
  A continuació, l’aplicació va demanant números i va responent si el nombre a endevinar és més gran o més petit que l'introduït,
  a més dels intents que et queden (tens 10 intents per encertar-lo).
  El programa acaba quan s'encerta el número (a més et diu quants intents has necessitat per encertar-lo),
  si s'arriba al límit d'intents, l’aplicació et mostra el número que havia generat.
  Pista: randint

"""
import random

numR = random.randint(1,100)
MAX_INTENTS = 10

for x in range(MAX_INTENTS):
    numero = int(input("Escull un numero: "))
    if numero < numR:
        print("Ha de ser més gran!")
    elif numero > numR:
        print("Ha de ser més petit!")
    else:
        print("L'has encertat!!")
        break
    MAX_INTENTS = MAX_INTENTS - 1
    print("Et queden " + str(MAX_INTENTS) + " intents!\n")
    if MAX_INTENTS == 0:
        print(f"El nombre era {numR}")
