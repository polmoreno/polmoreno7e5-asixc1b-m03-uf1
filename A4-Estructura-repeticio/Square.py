"""
  Pol Moreno

  30/11/2021

  ASIXc1A M03 UF1

  Square
  Mostrar per pantalla un quadrat de mida N (la triarà l'usuari)
  Cal "printar"  #  a les vores. És a dir, dalt, baix dreta i esquerra

"""

number = int(input("Escriu un numero: "))

for fila in range(number):
    for columna in range(number):
        if fila == 0 or fila == number - 1 or columna == 0 or columna == number - 1:
            print("#  ", end="")
        else: print("   ", end="")
    print()

print()

for fila in range(number):
    for columna in range(number):
        if fila == 0 or fila == number - 1 or columna == 0 or columna == number - 1:
            print("O  ", end="")
        else: print("X  ", end="")
    print()
