"""
  Pol Moreno

  23/11/2021

  ASIXc1A M03 UF1

  Exercici 19

  Escriu un programa que mostri el següent menú d’opcions:
  Literatura
  Cinema
  Música
  Videojocs
  Sortir

  Si l’usuari tria una opció de l’1 al 4, el programa ha de mostrar  uns quants suggeriments de títols relacionats amb
  el tema escollit. Si l’usuari tria una opció no contemplada, el programa ha de mostrar un missatge d’error.
  En tot cas, el programa tornarà a mostrar el menú d’opcions, tret de què l’usuari escolli l’opció 5:
  en aquest cas, el programa mostrarà un missatge de comiat i acabarà.

"""

opcio1 = "literatura"
opcio2 = "cinema"
opcio3 = "música"
opcio4 = "videojocs"
exit = "Sortir"
pregunta = 0

while pregunta != 5:
    print("-"*60)
    print("Escull un tema: \n\t1. Literatura \n\t2. Cinema \n\t3. Música \n\t4. Videojocs \n\t5. Sortir")
    pregunta = int(input("\nEscull un numero: (5 per sortir)  "))
    if pregunta == 1:
        print(f"Com que has escollit {opcio1} et recomano: \n\tEl Quijote - Miguel de Cervantes \n\tLa Odisea - Homero \n\tLa Divina Comedia - Dante Alighieri \n\tHamlet - William Shakespeare")
    elif pregunta == 2:
        print(f"Com que has escollit {opcio2} et recomano: \n\tEl Padrino (1972) \n\tEl mago de Oz (1939) \n\tCiudadano Kane (1941) \n\tPulp Fiction (1994)")
    elif pregunta == 3:
        print(f"Com que has escollit {opcio3} et recomano: \n\tBound 2 - Kanye West \n\tThat's On Me - Mac Miller \n\tYlang Ylang - FKJ \n\tStrawberry Fields Forever - The Beatles")
    elif pregunta == 4:
        print(f"Com que has escollit {opcio4} et recomano: \n\tCS:GO Counter Strike Global Offensive \n\tFIFA 22 \n\tGolf It \n\tMinecraft")
    elif pregunta == 5:
        print("Sortint...")
    else:
        print("Opció Incorrecte! \nTorna-ho a probar.")







