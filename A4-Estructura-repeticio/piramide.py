"""
  Pol Moreno

  01/12/2021

  ASIXc1A M03 UF1

  S'imprimeix una piràmide d'altura N de # centrada

"""
from time import sleep

altura = int(input("De quan ha de ser l'altura?  "))
base = (altura * 2) - 1
tronc = base // 2
altura_tronc = altura * 0.3

#arbre
for x in range(altura):
    if x == altura - 1:
        print(" " * (altura - x) + "# " * (x + 1))
    else:
        print(" " * (altura - x) + "#" + "·" * (2 * x + 1) + "#")
    #sleep(0.2)


#tronc
for i in range(round(altura_tronc)):
    print(" " * round(base * 0.4), end="")
    if altura % 2 == 0:
        print("|" * int(altura // 2))
    else:
        print("|" * int(altura // 2 + 1))
