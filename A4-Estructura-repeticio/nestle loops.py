"""
  Pol Moreno

  09/11/2021

  ASIXc1A M03 UF1

"""
#Using FOR

for i in range(1, 10):
    for j in range(1, 11):
        print("%d x %d = %d" % (i, j, i*j))
    print("--------------")


"""
#Using WHILE

contI = 1
contJ = 1

while contI <= 10:
    contJ = 0
    while contJ <= 10:
        print("%d x %d = %d" % (contI, contJ, contI*contJ))
        contJ += 1
    print("--------------")
    contI += 1"""


#Does it work?

