"""
  Pol Moreno

  23/11/2021

  ASIXc1A M03 UF1

  Exercici 18

  Fer un programa que mostri per pantalla un cronòmetre, indicant les hores, minuts i segons.
  Ajuda: feu servir la funció “sleep” del mòdul “time” per fer una pausa d’un segon de manera que a pantalla
  es vegi avançar el temps segon a segon.

"""
import os
from time import sleep

hores = 0
minuts = 0
segons = 0

while True:
    if segons == 60:
        segons = 0
        minuts += 1
        if int(minuts) == 60:
            minuts = 0
            hores += 1
        if int(hores) == 24:
            segons = 0
            minuts = 0
            hores = 0
    if segons < 10:
        segons = ("0" + str(segons))
    if int(minuts) < 10:
        minuts = ("0" + str(minuts))
    if int(hores) < 10:
        hores = ("0" + str(hores))

    print(f"{hores}:{minuts}:{segons}")
    segons = int(segons) + 1
    hores = int(hores)
    minuts = int(minuts)
    segons = int(segons)

    sleep(1)
    #os.system("cls")

