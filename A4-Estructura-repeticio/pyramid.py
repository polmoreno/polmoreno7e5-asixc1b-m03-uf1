"""
  Pol Moreno

  10/11/2021

  ASIXc1A M03 UF1

    S'imprimeix una piràmide d'altura N de #
"""

number = int(input("Escriu un numero: "))

for i in range(1, number + 1):
    print("#" * i)

number = int(input("Escriu un numero: "))

for i in range(number, 0, -1):
    print("#" * i)
