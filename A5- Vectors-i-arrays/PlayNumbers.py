"""
  Pol Moreno

  21/12/2021

  ASIXc1A M03 UF1

  PlayNumbers

  Volem fer un petit programa per guardar l'alineació inicial de jugadors d'un partit de bàsquet.
  L'usuari introduirà els 5 números dels jugadors. Imprimeix-los després amb el format indicat

"""

playersllista = []

for i in range(5):
    player = int(input("Dorsal jugador: "))
    if player > 9 and player < 100:
        playersllista.append(player)

playersllista.sort(reverse=True)
print(playersllista)

