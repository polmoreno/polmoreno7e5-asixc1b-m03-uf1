"""
  Pol Moreno

  12/01/2022

  ASIXc1A M03 UF1

  ScoreTable

  Demanar el nom de 3 Players
  Demanar el resultat de la partida de cada player [1..10]
  Mostrar els players en ordre alfabetic
  Mostrar els players en ordre de puntuació amb la seva puntuació al costat

"""

MAX_PLAYERS = 3
noms = []
score = []

for x in range(MAX_PLAYERS):
    noms.append(input("Nom dels players: "))

for x in range(MAX_PLAYERS):
    score.append(input(f"Puntuació de 1 al 10 de {noms[x]}: "))

print("El resultat és: ")
for x, y in sorted(zip(noms,score)):
    print(x, y)

for x, y in sorted(zip(score,noms)):
    print(x,y)
