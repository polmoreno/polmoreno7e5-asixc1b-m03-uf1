"""
  Pol Moreno

  21/12/2021

  ASIXc1A M03 UF1

  Day Of Week

  Donat un enter, mostrar el dia de la setmana amb text (dilluns, dimarts, dimecres…),
  tenint en compte que dilluns és el 0. Els dies de la setmana es guarden en una llista

"""

diesSetmana = ['Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte', 'Diumenge']

pregunta = int(input("Quin dia de la setmana vols amb numero? "))

print(diesSetmana[pregunta - 1])
