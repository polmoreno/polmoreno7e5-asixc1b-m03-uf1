"""
  Pol Moreno

  26/01/2022

  ASIXc1A M03 UF1

  Calcular Lletra DNI

  L’objectiu d’aquest exercici és crear un programa que utilitzi un diccionari per calcular el DNI.
  A la Web del Ministerio del Interior es pot trobar l’explicació de com fer-ho.
  Els passos a seguir, bàsicament són:
  Es divideix el nombre entre 23 i la resta se substitueix per una lletra que es determina per inspecció mitjançant la següent taula:

"""

DNI = [0, "T", 1, "R", 2, "W", 3, "A", 4, "G", 5, "M", 6, "Y", 7, "F", 8, "P", 9, "D", 10, "X", 11, "B",
       12, "N", 13, "J", 14, "Z", 15, "S", 16, "Q", 17, "V", 18, "H", 19, "L", 20, "C", 21, "K", 22, "E"]

dniNum = int(input("DNI? "))
dniLletra = input("LLetra")

