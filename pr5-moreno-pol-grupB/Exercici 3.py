"""
  Pol Moreno

  18/01/2022

  ASIXc1A M03 UF1

  Exercici 3

  Programa de traducció d’insults. Crear un array de dues dimensions amb els insults en català i afegir la traducció en castellà, anglès i klingon
  El programa, demanarà a l’usuari que escrigui per teclat un insult, en català, i el mostrarà traduït a castellà, anglès  i klingon.

  Exemple:
  llista=[[1,2,3],[4,5,6],[7,8,9]]
  ...
  print(insults[0][0]) #Mocós
  print(insults[0][1]) #Capsigrany

"""

insults = [["MOCÓS", "MOCOSO", "BRAT", "QEV"],
           ["CAPSIGRANY", "ALCAUDON", "SHRIKE", "DEGHWA"],
           ["MALPARIT", "BASTARDO", "BASTARD", "ROD"],
           ["LLEPACULS", "LAMECULOS", "TOADY", "DAHJAJ"],
           ["BENEIT", "BOBALICÓN", "GOOFY", "JAH"]]
cont = 0
diccionariCat = "Diccionari en català:\nMOCÓS\nCAPSIGRANY\nMALPARIT\nLLEPACULS\nBENEIT"
print(diccionariCat)
pregunta = input("\nDigues un insult del diccionari en català: ")

for fila in range(len(insults)):
    for columna in range(len(insults)):
        if pregunta.upper() in insults[cont]:
            print(f"Castellà: {insults[cont][1]} \nAnglés: {insults[cont][2]} \nKlingon: {insults[cont][3]}")
        break
    cont += 1
else:
    print("Aquest insult no està al diccionari.")
