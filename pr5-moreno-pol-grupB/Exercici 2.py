"""
  Pol Moreno

  18/01/2022

  ASIXc1A M03 UF1

  Exercici 2

  Programa que generi una llista de 100 nombres aleatoris entre 1 i 50. Obtenir la mitja dels nombres que es troben
  a les posicions parelles i la mitja del nombre de les  posicions senars.

  Per aconseguir nombres aleatoris en Python podem utilitzar la funció random.randint(limitInferior,limitSuperior)
  # Program to generate a random number between 0 and 9
  # importing the random module

"""

import random

VOLTA = 100
myList = []
par = []
senar = []
cont = 0

for i in range(VOLTA):
    aleatori = random.randint(1, 51)
    myList.append(aleatori)

for x in myList:
    if cont % 2 == 0:
        par.append(x)
    else:
        senar.append(x)
    cont += 1

mitjanaPar = sum(par) / len(par)
mitjanaSenar = sum(senar) / len(senar)

print(mitjanaPar)
print(mitjanaSenar)
