"""
  Pol Moreno

  18/01/2022

  ASIXc1A M03 UF1

  Exercici 1

  Programa de càlcul de temperatures emmagatzemades dins d’una llista de nombres sencers (sense decimals).
  Proveu el resultat usant les definicions següents d’array, una per a cada execució diferent:

  //Primer proveu si funciona usant aquesta llista
    gener = [1, 12, 4, -5, 7, 3, 2,
            9, -6, 7, 8, 2, 14, -7,
            5, 13, 12, 4, 6, -7, 1,
            12, 4, -2, 3, 5, 2, 7,
            -2, 8, -15]

  Cal definir els 3 primers mesos de l’any, i calcular:
  Per a cada mes: temperatura màxima, mínima i mitjana
  Per al 1r trimestre de l’any: temperatura màxima, mínima i mitjana

"""
gener = [1, 12, 4, -5, 7, 3, 2, 9, -6, 7, 8, 2, 14, -7, 5, 13,
         12, 4, 6, -7, 1, 12, 4, -2, 3, 5, 2, 7, -2, 8, -15]
febrer = [1, 12, 4, -5, 7, 3, 2, 9, -6, 7, 8, 2, 14, -7, 5, 13,
         12, 18, 6, -7, 1, 12, 4, -2, 3, 5, 2, 7, -2, 8, -20]
març = [1, 12, 4, -5, 25, 3, 2, 9, -6, 7, 8, 2, 14, -7, 5, 13,
         12, 4, 6, -22, 1, 12, 4, -2, 3, 5, 2, 7, -2, 8, -15]
trimestre = gener + febrer + març
suma = 0

MIN = gener[0]
MAX = gener[0]

for i in gener:
    if i < MIN:
        MIN = i
    if i > MAX:
        MAX = i
    suma += i
    MITJANA = suma / len(gener)
print(f"Tempreatures del gener: \n Màxima: {MAX} \n Mínima: {MIN} \n Mitjana: {round(MITJANA)}")


MIN = febrer[0]
MAX = febrer[0]

for i in febrer:
    if i < MIN:
        MIN = i
    if i > MAX:
        MAX = i
    suma += i
    MITJANA = suma / len(febrer)
print(f"\nTempreatures del febrer: \n Màxima: {MAX} \n Mínima: {MIN} \n Mitjana: {round(MITJANA)}")


MIN = març[0]
MAX = març[0]

for i in març:
    if i < MIN:
        MIN = i
    if i > MAX:
        MAX = i
    suma += i
    MITJANA = suma / len(març)
print(f"\nTempreatures del març: \n Màxima: {MAX} \n Mínima: {MIN} \n Mitjana: {round(MITJANA)}")

MIN = trimestre[0]
MAX = trimestre[0]
