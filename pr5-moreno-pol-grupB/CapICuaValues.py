"""
  Pol Moreno

  26/01/2022

  ASIXc1A M03 UF1

  Cap I Cua Values

  Mostrar per pantalla cap i cua si la llista de N valors introduïts per l'usuari
  són cap i cua (llegits en ordre invers és la mateixa llista).

"""

quantitat = int(input("Quantitat: "))
numeros = []

for x in range(quantitat):
    numeros.append(int(input("Escriu els numeros: ")))
if numeros == numeros[::-1]:
    print("Cap i Cua.")
else:
    print("No ho és.")

