"""
  Pol Moreno

  26/01/2022

  ASIXc1A M03 UF1

  Palindrum

  Indica si una paraula és palíndrom.

  Exemples de palíndroms:

  cec, cuc, gag, mínim, nan, nen, pipiripip…

"""

paraula = input("Escriu una paraula: ")

if paraula == paraula[::-1]:
    print("Palindrum.")
else:
    print("No és un palindrum.")

"""
paraula = input("Escriu una paraula: ")
paraula_reverse = paraula[::-1]

for x in range(len(paraula)/2):
    if paraula[x] != paraula_reverse[x]:
        print("No és un palindrum.")
"""


"""
paraula = input("Escriu una paraula: ")
lletresIguals = True
cont = 0
contInvers = -1

while cont <= len(paraula)/2 and lletresIguals:
    if paraula[cont] != paraula[-1]:
        lletresIguals = False
    print(paraula[cont])
    cont += 1
    contInvers -= 1
if lletresIguals:
    print("Palindrum.")
else:
    print("No és un palindrum.")
"""
