"""
  Pol Moreno

  01/02/2022

  ASIXc1A M03 UF1

  Exercici 3 ENCRIPTAR FRASES (4p)

  Feu un programa per llegir una frase pel teclat i, en acabar, mostri la frase encriptada.
  Per encriptar la frase, ha de fer servir la posició de cadascuna de les lletres que hauran d’estar emmagatzemades.
  Els espais en blanc no s’han de codificar i entre el codi numèric de cada lletra, caldrà escriure el caràcter :
  per poder identificar a on comença i acaba cada lletra. No fer diferències entre majúscules i minúscules.

  Exemple:
  Aaa  Bbb  Mmmab
  0:0:0 1:1:1 12:12:12:0:1

"""

llista = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
frase = input("Escriu la frase: ")

for lletra in frase:
    if lletra.lower() in llista:
        print(f"{llista.index(lletra.lower())}:",end="")
    else:
        print(f"{lletra}:", end="")


