"""
  Pol Moreno

  01/02/2022

  ASIXc1A M03 UF1

  Exercici 2 VORES DE LA MATRIU (4p)

  Es demana el nombre de files i columnes d’una matriu. Comprovar que la matriu sigui quadrada,
  és a dir que tingui les mateixes fileres que columnes. Omplir la matriu amb 1’s a les vores i la resta amb 0`s.
  Mostrar la matriu resultant. S'entén per vores de la matriu la primera i l’última filera i la primera i última columna.

"""

files = int(input("Quantes files vols? "))
columnes = int(input("Quantes columnes vols? "))

while files != columnes or files % 2 == 0 and columnes % 2 == 0:
    print("\nTorna-ho a provar, el numero de files i columnes han de ser iguals i senars.")
    files = int(input("\nQuantes files vols? "))
    columnes = int(input("Quantes columnes vols? "))

print()

for fila in range(files):
    for columna in range(columnes):
        if fila == 0:
            print("1 ",end="")
        elif columna == 0:
            print("1 ",end="")
        elif fila == files - 1:
            print("1 ",end="")
        elif columna == columnes - 1:
            print("1 ",end="")
        else:
            print("0 ",end="")
    print()