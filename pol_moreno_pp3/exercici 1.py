"""
  Pol Moreno

  01/02/2022

  ASIXc1A M03 UF1

  Exercici 1 PARAULES (2p)

  Crea una llista, omple-la pel teclat amb 10 paraules. Mostra tot el seu contingut. Calcula la paraula més llarga,
  la més curta i la mida mitjana de les paraules incloses. Mostra els càlculs per pantalla.

"""

NUM_PALABRAS = 10
llista = []

for i in range(NUM_PALABRAS):
    paraula = input("Afageix una paraula: ")
    llista.append(paraula)
    print(llista)

paraulaLlarga = llista[0]
contadorLargo = 0

for j in llista:
    if len(paraulaLlarga) < len(llista[contadorLargo]):
        paraulaLlarga = llista[contadorLargo]
    contadorLargo = contadorLargo + 1

paraulaCurta = llista[0]
contadorCorto = 0

for x in llista:
    if len(paraulaCurta) > len(llista[contadorCorto]):
        paraulaCurta = llista[contadorCorto]
    contadorCorto = contadorCorto + 1

contador = 0
medida = 0

for y in llista:
    medida = medida + len(llista[contador])
    contador = contador + 1
medida = float(medida /len(llista))

print("La paraula llarga es: " + paraulaLlarga)
print("La paraula corta es:  " + paraulaCurta)
print("La mitjana de las paraules es: " + str(medida))