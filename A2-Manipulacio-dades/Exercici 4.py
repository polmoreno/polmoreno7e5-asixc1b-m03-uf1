"""
    Pol Moreno

    06/10/2021

    ASIXcB M03 UF1 A2

    Exercici 4
    Escriu el programa que calcula i mostra en pantalla la suma, resta, divisió i multiplicació de dos nombres entrats per l'usuari.

"""

numero1 = float(input("Entra el primer numero de l'operació: "))
numero2 = float(input("Entra el segon numero de l'operació: "))

suma = str(numero1 + numero2)
print("Aquesta és la suma: " + str(suma))

resta = str(numero1 - numero2)
print("Aquesta és la resta: " + str(resta))

divisio = str(numero1 / numero2)
print("Aquesta és la divisió: " + str(divisio))

multiplicacio = str(numero1 * numero2)
print("Aquesta és la multipicació: " + str(multiplicacio))
