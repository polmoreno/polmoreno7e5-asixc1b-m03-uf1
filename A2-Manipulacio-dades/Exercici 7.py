"""
    Pol Moreno

    06/10/2021

    ASIXcB M03 UF1 A2

    Exercici 7
    Implementa un programa que llegeixi del teclat una quantitat de minuts i després mostri en pantalla a quantes hores i minuts correspon aquesta quantitat.

"""

qminuts = int(input("Quantitat de minuts: "))
hores = qminuts // 60
minuts = qminuts % 60

print("Això són " + str(hores) + " hores i " + str(minuts) + " minuts")
