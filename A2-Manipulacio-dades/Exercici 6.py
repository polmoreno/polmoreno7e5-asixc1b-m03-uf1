"""
    Pol Moreno

    06/10/2021

    ASIXcB M03 UF1 A2

    Exercici 6
    Escriu un programa que demani a l'usuari entrar tres nombres i després escrigui a la pantalla la seva mitjana.

"""

numero1 = float(input("Tria un numero: "))
numero2 = float(input("Tria un altre numero: "))
numero3 = float(input("Tria un altre numero: "))

mitjana = ( numero1 + numero2 + numero3) // 3
print(int(mitjana))
