"""
    Pol Moreno

    13/10/2021

    ASIXcB M03 UF1 A2

    Exercici 20
    Escriure un programa que a partir del nombre de monedes que tenim dels següents tipus: 2 €, 1 €, 50 cèntims,
    20 cèntims i 10 cèntims, ens digui l'import corresponent en euros i cèntims.

"""

monedes_2 = int(input("Monedes de 2€: "))
monedes_1 = int(input("Monedes de 1€: "))
monedes_050 = int(input("Monedes de 0.5€: "))
monedes_020 = int(input("Monedes de 0.2€: "))
monedes_010 = int(input("Monedes de 0.1€: "))

import_total = (monedes_1 * 1) + (monedes_2 * 2) + (monedes_010 * 0.1) + (monedes_020 * 0.2) + (monedes_050 * 0.5)

print(f" Tens {round(import_total, 2)}€")
