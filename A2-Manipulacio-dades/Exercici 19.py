"""
    Pol Moreno

    13/10/2021

    ASIXcB M03 UF1 A2

    Exercici 19
    Implementa el programa que calculi la nota d'un test realitzat per un estudiant.
    La nota es calcularà així: per cada resposta correcta se sumaran cinc punts, per cada incorrecta es restarà
    un punt i les respostes en blanc ni sumaran ni restaran. El programa demanarà les dades estrictament necessàries i
    mostrarà en pantalla la nota del test.

"""

correcte = int(input("Respostes correctes: "))
incorrecte = int(input("Respostes incorrectes: "))

nota = (correcte * 5) - (incorrecte * 1)

print(f"PUNTUACIÓ: {nota}")
