"""
    Pol Moreno

    13/10/2021

    ASIXcB M03 UF1 A2

    Exercici 15
    Escriu el programa que demani a l'usuari el valor de dues variables A i B, intercanviï els seus valors i finalment els mostri.

"""

variableA = input("Tria la variable A: ")
variableB = input("Tria la variable B: ")

print(variableA, variableB)

temp = variableB
variableB = variableA
variableA = temp

print(variableA, variableB)
