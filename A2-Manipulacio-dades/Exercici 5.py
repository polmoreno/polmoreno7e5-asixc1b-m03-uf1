"""
    Pol Moreno

    06/10/2021

    ASIXcB M03 UF1 A2

    Exercici 5
    Realitza un programa que demani a l'usuari graus Fahrenheit i posteriorment els mostri com graus Celsius.

    La fórmula per a la conversió és:
    C = (F-32)*5/9sjo

"""

fahrenheit = float(input("Graus Fahrenheit: "))

celsius = (fahrenheit - 32) * 5/9
print("Els graus Fahrenheit que has escollit són equivalents a " + str(celsius) + " Celsius")
print("%.2f Fahrenheit són %.2f Celsius"%(fahrenheit, celsius))   #%.2f vol dir % la variable que definim al final, 2 són els decimals i la f és de float.
