"""
    Pol Moreno

    13/10/2021

    ASIXcB M03 UF1 A2

    Exercici 18
    Escriu un programa que donat el nom i els dos cognoms introduïts per un usuari, mostri les inicials
    corresponents en majúscules.

"""

nom = input("Nom i Congnoms: ")

paraula = nom.split(" ")
character = ""
for lletra in paraula:
    character += lletra[0]

print("Les teves inicals son " + character.upper())
