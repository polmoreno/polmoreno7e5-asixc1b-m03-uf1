"""
    Pol Moreno

    05/10/2021

    ASIXcB M03 UF1 A2

    Exercici 3
    Donats els catets a i b d'un triangle rectangle, escriure el codi Python que calcula la seva hipotenusa. Recordar que la hipotenusa es calcula de la següent manera (Teorema de Pitàgores):
    c2 = a2 + b2 , on c és la hipotenusa. Per tant  c = √(a2 + b2)
"""
import math

catetA = float(input("Tria un catet d'un triangle rectangle: "))
catetB = float(input("Tria l'altre catet d'un triangle rectangle: "))
base = float(input("Tria la base del triangle rectangle: "))
altura = float(input("Tria l'altura del triangle rectangle: "))

c = math.sqrt(catetA**2 + catetB**2)    #També es pot fer així: c = math.sqrt(pow(catetA,2) + pow(catetB,2))
print("L'hipotenusa del triangle rectangle és " + str(round(c, 2)))
