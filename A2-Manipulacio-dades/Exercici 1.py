"""
    Pol Moreno

    05/10/2021

    ASIXcB M03 UF1 A2

    Exercici 1
    Escriure un programa que pregunti a l'usuari pel seu nom de pila, i tot seguit escriviu el missatge "Hola, XXX!" on XXX és el nom de pila introduït per l'usuari. Per exemple, si l'usuari entra "Javi", el missatge serà "Hola, Javi!"

"""

nom = input("Quin és el teu nom de pila? ")
print("Hola " + nom + "!")
