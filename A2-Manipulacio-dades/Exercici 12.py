"""
    Pol Moreno

    13/10/2021

    ASIXcB M03 UF1 A2

    Exercici 12
    Implementa un programa que demani per teclat dos punts del pla i tot seguit calculi i mostri la distància entre ells.

    Notes:

    Un punt del pla es representa amb un parell de números. Per exemple, el punt A es representarà pels números x1 i y1, i el punt B pels números x2 i y2
    La fórmula de la distància entre dos punts A i B és:

    distància(A,B) = √( (x2 - x1)2 + (y2 - y1)2)

"""
import math

x1 = int(input("Tria el punt x1: "))
y1 = int(input("Tria el punt y1: "))
x2 = int(input("Tria el punt x2: "))
y2 = int(input("Tria el punt y2: "))

distancia = abs(math.sqrt(pow((x2 - x1),2) + pow((y2 - x2),2)))

print("La distancia entre els dos punts del pla és " + str(distancia))
