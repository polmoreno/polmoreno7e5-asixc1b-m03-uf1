"""
    Pol Moreno

    13/10/2021

    ASIXcB M03 UF1 A2

    Exercici 10
    En un institut la nota de l'assignatura de Programació es calcula de la següent manera:

     • 55% de la mitjana de tres qualificacions parcials.
     • 30% de la nota d’una prova final.
     • 15% de la nota d’un treball final.

    Implementa un programa que calculi la nota final a partir dels valors entrats per teclat corresponents a les qualificacions parcials i notes de la prova i treball finals.

"""

parcial1 = float(input("Nota primer parcial: "))
parcial2 = float(input("Nota segon parcial: "))
parcial3 = float(input("Nota tercer parcial: "))
provaFinal = float(input("Nota de la prova final: "))
treballFinal = float(input("Nota del treball final: "))

parcials = ((parcial1 + parcial2 + parcial3) // 3) * 0.55
prova = provaFinal * 0.3
treball = treballFinal * 0.15
total = parcials + prova + treball

print("La nota final de l'assignatura és " + str(round(total,2)))
