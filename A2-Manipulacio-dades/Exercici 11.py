"""
    Pol Moreno

    13/10/2021

    ASIXcB M03 UF1 A2

    Exercici 11
    Realitza un programa que mostri la "distància" entre dos nombres entrats per teclat.

    Nota: la "distància entre dos nombres" és el valor absolut de la seva diferència.

    Per exemple: distància (5,3) = / 5 - 3 / = 2, distància (15,22) = / 15 - 22 / = 7

"""

distanciaA = float(input("La distància A és: "))
distanciaB = float(input("La distància B és: "))

distancia = abs(distanciaA - distanciaB)

print("La distancia del punt A al punt B és de " + str(distancia))
