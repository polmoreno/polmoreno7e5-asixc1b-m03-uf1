"""
    Pol Moreno

    06/10/2021

    ASIXcB M03 UF1 A2

    Exercici 8
    Un venedor percep cada mes un sou base més un 10% de l'import de cada venda realitzada al mes en concepte de comissió de vendes.

    Realitza un programa que a partir d'un import de sou base i l'import de tres vendes, calculi i escriu en pantalla:

        -l'import percebut en concepte de comissió de vendes
        -l'import total percebut pel venedor (el salari)

    Nota: els imports de salari base i de les tres vendes seran entrats per teclat.

"""

salariBase = float(input("Quin és el teu salari base? "))

venta1 = float(input("Primera venta de "))
venta2  = float(input("Segona venta de "))
venta3  = float(input("Tercera venta de "))

comissio = float(0.1 * (venta1 + venta2 + venta3))
salari = salariBase + comissio

print(("\nTens una comissió de " + str(round(comissio,2)) + "€ per tant, un salari total de " + str(salari) + "€"))
