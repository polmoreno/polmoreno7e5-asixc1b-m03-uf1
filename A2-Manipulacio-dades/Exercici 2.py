"""
    Pol Moreno

    05/10/2021

    ASIXcB M03 UF1 A2

    Exercici 2
    Donada la base i altura d'un rectangle, escriure el programa que calcula i mostra el seu perímetre i àrea.

"""

base = float(input("Tria la base del rectangle: "))
altura = float(input("Tria l'altura del rectangle: "))

perimetre = 2 * (base + altura)
area = base * altura

print("El perimetre del rectangle és " + str(perimetre))
print("L'àrea del rectangle és " + str(round(area,2)))
