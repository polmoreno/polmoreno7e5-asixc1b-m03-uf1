"""
    Pol Moreno

    13/10/2021

    ASIXcB M03 UF1 A2

    Exercici 13
    Escriu un programa que llegeixi un nombre i tot seguit calculi i mostri l'arrel quadrada i cúbica d'aquest número.
    Desafortunadament, Python3 no té cap funció predefinida que calculi l'arrel cúbica, però sí que pots utilitzar l’operador que “fa l’operació inversa” …

"""

import math

numero = float(input("Introduiex un número: "))

arrel2 = math.sqrt(numero)
arrel3 = numero ** (1. / 3.)

print()
print(f"Arrel cuadrada {round(arrel2, 3)} i arrel cubica {round(arrel3, 3)}")

