"""
    Pol Moreno

    06/10/2021

    ASIXcB M03 UF1 A2

    Exercici 9
    Escriu un programa que donat l'import d'una compra, apliqui a aquest import un descompte del 15% i el mostri a pantalla.

"""

compra = float(input("Quin és l'import de la teva compra? "))
descompte = float(compra * 0.15)
descompteTotal = float(compra - descompte)

print("Tens un descompte de " + str(round(descompte,2)) + "€ per tant el preu final és de " + str(round(descompteTotal,2)) + "€")
