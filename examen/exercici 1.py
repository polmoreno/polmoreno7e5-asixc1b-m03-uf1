"""
  Pol Moreno

  15/12/2021

  ASIXc1A M03 UF1

  Exercici 1

  SumPositiveValues

"""

quantitat = int(input("Quants enters escriuràs? "))
contador = 0

for x in range(quantitat):
    llista = int(input("Escriu els nombres enters: "))
    if llista >= 0:
        contador += llista
print(contador)
