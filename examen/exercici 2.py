"""
  Pol Moreno

  15/12/2021

  ASIXc1A M03 UF1

  Exercici 2

  MyReplace

"""
from time import sleep

pregunta = str(input("Introdueix una frase?\n"))
caracterCanvi = input("Quin caràcter vols remplaçar?\n")
caracterSubstitucio = input("Quin és el caràcter de substitució?\n")


for lletra in pregunta:
    if lletra == caracterCanvi:
        lletra = caracterSubstitucio
    print(lletra, end="")
    sleep(1)

#O BÉ
"""
for lletra in pregunta.lower():
    if lletra == caracterCanvi.lower():
        lletra = caracterSubstitucio.lower()
    print(lletra, end="")
"""
