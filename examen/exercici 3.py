"""
  Pol Moreno

  15/12/2021

  ASIXc1A M03 UF1

  Exercici 3

  Matrix

"""
#Recordar que va del 0 al 7 ja que sóm informatics i començem a contar sempre amb el zero.

BLANC = " B "
NEGRE = " N "
MOVIMENT = " * "
matriu = 8

print("El tauler és de 8x8.")

fila = int(input("FILA: "))
columna = int(input("COLUMNA: "))

for x in range(matriu):
    for i in range(matriu):
        if x == fila or i == columna:
            print(MOVIMENT, end="")
        elif (x + i) % 2 == 0:
            print(BLANC, end="")
        else:
            print(NEGRE, end="")
    print()
