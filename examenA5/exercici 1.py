"""
  Pol Moreno

  01/02/2022

  ASIXc1A M03 UF1

  Exercici 1

  Fer un programa per demanar el número de files i columnes, d'una matriu. Comprovar que la matriu sigui quadra, és a dir
  que tingui les mateixes fileres que columnes. I que sigui un número senar de fileres i columnes. Omplir la matriu amb 0's
  tret de la primera i última columna i la filera central, que s'han d'omplir amb 1's. Mostrar la matriu resultant per pantalla.
  El resultat ha de dibuixar una H amb els 1's dins de la matriu de 0's

"""

"""fila = int(input("Cuantas filas quieres en la matriz: "))
columna = int(input("Cuantas columnas quieres en la matriz: "))

if fila == columna and fila %2 != 0 and columna %2 != 0:
    for i in range(fila):
        for j in range(columna):
            if i == (fila//2):
                print("1 ",end="")
            elif j == 0:
                print("1 ",end="")
            elif j == columna -1:
                print("1 ",end="")
            else:
                print("0 ",end="")
        print()

else:
    print("Has de poner bien las medidas ")"""




fila = int(input("Quantes files vols? "))
columna = int(input("Quantes columnes vols? "))

while fila != columna or fila % 2 == 0 and columna % 2 == 0:
    print("\nTorna-ho a provar, el numero de files i columnes han de ser iguals i senars.")
    fila = int(input("Quantes files vols? "))
    columna = int(input("Quantes columnes vols? "))

print()

for y in range(columna):
    for x in range(fila):
        if y == (columna // 2):
            print("1 ", end="")
        elif x == 0 or x == fila - 1:
            print("1 ", end="")
        else:
            print("0 ", end="")
    print("")
