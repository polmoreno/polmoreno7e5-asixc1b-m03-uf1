"""
  Pol Moreno

  01/02/2022

  ASIXc1A M03 UF1

  Exercici 2

  Feu un programa que vagi llegint frases pel teclat i, en acabar cada entrada d'una frase, mostri la frase encriptada.
  Per encriptar la frase, ha de canviar les vocals per numeros del 1 al 5. No fer diferències entre majúsucles i minúscules
  ni lletras amb accents, totes s'han de tractar com si fossin minúscules i sense accent.

"""

un = ["a", "á", "à"]
dos = ["e", "é", "è"]
tres = ["i", "í", "ì", "ï"]
quatre = ["o", "ó", "ò"]
cinc = ["u", "ú", "ù", "ü"]

frase = input("Escriu la frase: ")

for lletra in frase:
    if lletra in un:
        print("1", end="")
    elif lletra in dos:
        print("2", end="")
    elif lletra in tres:
        print("3", end="")
    elif lletra in quatre:
        print("4", end="")
    elif lletra in cinc:
        print("5", end="")
    else:
        print(lletra, end="")

