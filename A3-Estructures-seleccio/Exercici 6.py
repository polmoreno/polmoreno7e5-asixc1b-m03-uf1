"""
    Pol Moreno

    26/10/2021

    ASIXcB M03 UF1 A3

    Exercici 6
    Realitza un programa que demani una cadena per teclat, que comprovi si està escrita en majúscules o no i aleshores mostri un missatge en conseqüència.

"""

frase = input("Frase: ")

if frase != frase.upper():
    print("La frase no està tota en majuscules.")
else:
    print("La frase està en majuscules.")
