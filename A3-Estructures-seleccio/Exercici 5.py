"""
    Pol Moreno

    26/10/2021

    ASIXcB M03 UF1 A3

    Exercici 5
    Realitza un programa que demani un nom d'usuari i una contrasenya. Si l'usuari és "pepe" i la contrasenya "asdasd", el programa mostrarà el missatge "Has entrat a el sistema".
    En cas contrari es mostrarà un missatge d'error (com per exemple, "Usuari / password incorrecte")

"""

usuari = input("Escull el nom d'usuari: ")
contrasenya = input("Escriu la contrasenya: ")

if usuari == "pepe" and contrasenya == "asdasd":
    print("Has entrat al sistema.")
elif usuari == "pepe" and contrasenya != "asdasd":
    print("Contrasenya incorrecte.")
elif contrasenya == "asdasd" and usuari != "pepe":
    print("Usuari incorrecte.")
else:
    print("Usuari i Contrasenya Incorrecte")
