"""
    Pol Moreno

    26/10/2021

    ASIXcB M03 UF1 A3

    Exercici 3
    Implementa un programa que demani a l'usuari un nombre per pantalla i indiqui si és parell o senar.

"""

num = int(input("Escull un nombre: "))

if num % 2 == 0:
    print("Aquest nombre és par.")
else:
    print("Aquest nombre és impar.")
