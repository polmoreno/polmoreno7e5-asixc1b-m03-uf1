"""
    Pol Moreno

    2/11/2021

    ASIXcB M03 UF1 A3

    Exercici 18
    Escriu un programa que donat un dia de la setmana (com a número, de l'1 a l'7), escrigui
    el dia corresponent en lletres. Si introduïm un nombre que no sigui de l'1 a l'7, el programa mostrarà un missatge d'error.

    Extra bonus: Modificar el programa per a tres idiomes: Anglès, Castellà i Català

"""

idioma = input("Quin idioma prefereixes, anglès, castellà o català? ")

if idioma.lower() == "català":
    diaSetmana = int(input("\nDigues un dia de la setmana amb numero (1-7): "))
    if diaSetmana == 1:
        print("Dilluns")
    elif diaSetmana == 2:
        print("Dimarts")
    elif diaSetmana == 3:
        print("Dimecres")
    elif diaSetmana == 4:
        print("Dijous")
    elif diaSetmana == 5:
        print("Divendres")
    elif diaSetmana == 6:
        print("Dissabte")
    elif diaSetmana == 7:
        print("Diumenge")

elif idioma.lower() == "castellà":
    diaSetmana = int(input("\nDime un día de la semana en numero (1-7): "))
    if diaSetmana == 1:
        print("Lunes")
    elif diaSetmana == 2:
        print("Martes")
    elif diaSetmana == 3:
        print("Miercoles")
    elif diaSetmana == 4:
        print("Jueves")
    elif diaSetmana == 5:
        print("Viernes")
    elif diaSetmana == 6:
        print("Sabado")
    elif diaSetmana == 7:
        print("Domingo")

elif idioma.lower() == "anglès":
    diaSetmana = int(input("\nType a day of the week with the number (1-7): "))
    if diaSetmana == 1:
        print("Monday")
    elif diaSetmana == 2:
        print("Tuesday")
    elif diaSetmana == 3:
        print("Wednesday")
    elif diaSetmana == 4:
        print("Thursday")
    elif diaSetmana == 5:
        print("Friday")
    elif diaSetmana == 6:
        print("Saturday")
    elif diaSetmana == 7:
        print("Sunday")
else:
    print("Incorrecte.")

