"""
    Pol Moreno

    26/10/2021

    ASIXcB M03 UF1 A3

    Exercici 4
    Escriu un programa que demani dos nombres per teclat i mostri la seva divisió.
    El programa ha de comprovar si el segon nombre és igual a zero.
    Si ho és en lloc de realitzar i mostrar el resultat de la divisió ha de mostrar un missatge d'error per pantalla indicant que la divisió no es pot realitzar, ja que el segon nombre és zero.

"""

num1 = float(input("Escull el primer numero:"))
num2 = float(input("Escull el segon numero:"))

if num2 == 0:
    print("ERROR! No es pot dividir entre 0")
else:
    divisio = num1 / num2
    print("El resultat de la divisió és " + str(divisio))
    print("El resultat de la divisió és %.2f" %(divisio))












