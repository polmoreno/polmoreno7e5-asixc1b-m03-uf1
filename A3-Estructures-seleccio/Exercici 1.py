"""
    Pol Moreno

    26/10/2021

    ASIXcB M03 UF1 A3

    Exercici 1
    Escriu un programa que demani per teclat dos nombres i mostri un missatge que digui si el primer és més gran que el segon o bé que no ho és.


"""

nombre1 = float(input("Escull el primer nombre: "))
nombre2 = float(input("Escull el segon nombre: "))

if nombre1 > nombre2:
    print("El primer nombre és més gran que el segon nombre.")
elif nombre1 == nombre2:
    print("Els dos nombres són iguals.")
else:
    print("El primer nombre és més petit que el segon nombre.")
