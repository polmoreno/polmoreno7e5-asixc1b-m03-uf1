"""
    Pol Moreno

    26/10/2021

    ASIXcB M03 UF1 A3

    Exercici 2
    Crea un programa que llegeixi un nombre per pantalla i mostri en pantalla un missatge que digui si és positiu, negatiu o igual a zero.

"""

numero = float(input("Introdueix un nombre: "))

if numero == 0:
    print("El nombre és igual a zero.")
elif numero > 0:
    print("El nombre és positiu.")
else:
    print("El nombre és negatiu.")
