"""
  Pol Moreno

  16/11/2021

  ASIXc1B M03 UF1

  Exercici 1

  Programa que demana a l'usuari la introducció de 10 nombres sencers i ha de mostrar,
  al final i per pantalla, quants són positius, quants negatius i quants zero.

"""
positius = 0
negatius = 0
zero = 0

for i in range (10):
    num = int(input("Escull 10 nombres sencers: "))
    if num > 0:
        positius = positius + 1
    elif num < 0:
        negatius = negatius + 1
    else:
        zero = zero + 1

print("\nPositius: " + str(positius))
print("Negatius: " + str(negatius))
print("Zero: " + str(zero))
