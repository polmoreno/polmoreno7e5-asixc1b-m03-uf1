"""
  Pol Moreno

  16/11/2021

  ASIXc1B M03 UF1

  Exercici 3

  Programa que mostra per pantalla la suma de tots els nombres senars
  i de tots els nombres parells inferiors a un número límit,
  que l’usuari introdueix per teclat. ( Ex: sí el límit és 31 sumaParells 240 i sumaSenars 225)

"""
num = int(input("Escull un numero: "))
parells = 0
senars = 0

for x in range(num):
    if x % 2 == 0:
        parells = parells + x
    else:
        senars = senars + x

print(parells)
print(senars)
