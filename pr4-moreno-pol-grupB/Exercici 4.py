"""
  Pol Moreno

  16/11/2021

  ASIXc1B M03 UF1

  Exercici 4

  Programa que imprimeix un tauler d’escacs per pantalla.
  Un taulell d’escacs comença amb la casella Blanca i és de mida 8x8 sempre ;-)

  Fer servir:
    blanc = "   "
    negre = "███"

"""
BLANC = "   "
NEGRE = "███"
num = 8

for x in range(num):
    for i in range(num):
        if (i+x) %2 == 0:
            print(BLANC, end="")
        else:
            print(NEGRE, end="")
    print()
