"""
  Pol Moreno

  16/11/2021

  ASIXc1B M03 UF1

  Exercici 2

  Programa que mostra per pantalla tots els nombres parells menors que 20.

"""

for num in range(20):
    if num % 2 == 0:
        print(num)

